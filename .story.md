---
focus: src/garage_service.py:13:old
---
### Do They Really Park Cars?
It all looks much better now, but there’s still one thing to fix: the name of the 
method [park\_in\_free\_garage](src/garage_service.py:13:old). 

As you can see from the implementation logic, this service doesn’t park cars but instead registers 
vehicles in a free garage depending on car type. There are no parking actions, the garage is simply 
chosen and cleaned. The best practice would be renaming the method to `register_in_garage` and 
renaming the method [register_car](src/garage_service.py:32) accordingly.

Now that the code is clean and readable, you might notice that the register method doesn’t process all 
possible options. For instance, what if the car was already registered? There’s no such check, and this 
could be a serious defect. 
But that’s another story.